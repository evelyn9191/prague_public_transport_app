# calendar.txt
# service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday, start_date, end_date
# 1, 0, 0, 0, 0, 0, 0, 1, 20190818, 20190818
# 69,1,1,1,1,1,0,0,20190828,20190828

# calendar_dates.txt
# 1 - Service has been added for the specified date.
# 2 - Service has been removed for the specified date.
# service_id,date,exception_type
# 0000001-2,20191109,1
# 0000011-2,20191109,2


# trips.txt
# block_id je konstruováno jako trip_id prvního spoje v sekvenci spojů, které na sebe navazují.
# Viz níže příklad spoje linky 174, který na Lukách mění číslo a pokračuje dále jako spoj linky 301 do Chýnice.
# Přes block_id jsou propojeny pouze spoje, kde vozidlo pokračuje dále a cestující tak nemusí přestupovat.
# Garantované návaznosti s přestupem zachycují transfers.txt.
# route_id, service_id, trip_id, trip_headsign, shape_id, wheelchair_accessible, block_id, direction_id, bikes_allowed, exceptional
# L991D1, 1, 1, "Nemocnice Motol", 1, 1,, 0, 0, 0
# L991D1, 1, 2, "Depo Hostivař", 2, 1,, 1, 0, 0
