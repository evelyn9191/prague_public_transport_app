import logging
from datetime import date, time
from itertools import groupby
from operator import attrgetter
from typing import Optional

from sqlalchemy import and_, func
from sqlalchemy.orm import Query

from database.db_models import (
    StopsVirtual,
    Stops,
    StopTimes,
    Trips,
    StopTimesSchema,
    StopsVirtualSchema,
    session,
)

logger = logging.getLogger(__name__)


def match_stop(stop: str) -> dict:
    matched_stops = (
        session.query(StopsVirtual)
        .with_entities(StopsVirtual.stop_name)
        .filter(StopsVirtual.stop_name.match(stop + "*"))
        .group_by(StopsVirtual.stop_name)
    )
    dumped_stops = StopsVirtualSchema(many=True).dump(matched_stops)
    return dumped_stops


def _filter_stop_ids(stop_name: str) -> Query:
    return Stops.query.with_entities(Stops.stop_id).filter(Stops.stop_name == stop_name).subquery()


def _filter_trip_ids(
    filtered_stop_ids: Query, travel_day: int, departure_time: time, arrival_time: Optional[time]
) -> Query:

    time_filter = (
        StopTimes.arrival_time <= arrival_time
        if arrival_time
        else StopTimes.departure_time >= departure_time
    )

    return (
        StopTimes.query.join(StopTimes.trips)
        .filter(
            and_(
                StopTimes.stop_id.in_(filtered_stop_ids),
                time_filter,
                func.substr(Trips.service_id, travel_day, 1) == "1",
            )
        )
        .subquery()
    )


def _filter_arrival_trip_ids_for_direct_trips(filtered_stop_ids: Query) -> Query:
    return (
        StopTimes.query.with_entities(StopTimes.trip_id).filter(
            StopTimes.stop_id.in_(filtered_stop_ids)
        )
    ).subquery()


# @use_kwargs(TripData())
def search_connection(
    departure_stop_name: str,
    arrival_stop_name: str,
    travel_date: date,
    travel_day: int,
    arrival_time: Optional[time],
    mode: int,
    transfer_counts: int,
    stop_included: Optional[str],
    departure_time: time,
) -> list:
    # route_type = mode: 0 tramvaj, 1 metro, 2 autobus, 3 vlak, 4 privoz. viz pathways

    departure_stop_ids = _filter_stop_ids(departure_stop_name)
    arrival_stop_ids = _filter_stop_ids(arrival_stop_name)

    departure_trip_ids = _filter_trip_ids(
        departure_stop_ids, travel_day, departure_time=departure_time, arrival_time=arrival_time
    )
    arrival_trip_ids = _filter_arrival_trip_ids_for_direct_trips(arrival_stop_ids)

    direct_trips = search_direct_connection(departure_trip_ids, arrival_trip_ids)

    if direct_trips:
        found_trip = transform_sql_output_to_dict(direct_trips)
        return found_trip

    return []


def search_direct_connection(departure_trip_ids: Query, arrival_trip_ids: Query) -> list:
    return (
        StopTimes.query.join(departure_trip_ids, StopTimes.trip_id == departure_trip_ids.c.trip_id)
        .filter(StopTimes.trip_id.in_(arrival_trip_ids))
        .order_by(departure_trip_ids.c.departure_time.asc())
    )


def transform_sql_output_to_dict(sql_results: list) -> list:
    direct_trip_pairs = [list(g) for k, g in groupby(sql_results, attrgetter("trip_id"))]
    connections = []
    for connection in direct_trip_pairs:
        connection_stops = []
        for stop in connection:
            dumped_stop = StopTimesSchema().dump(stop)
            connection_stops.append(dumped_stop)
        connections.append(connection_stops)
    return connections
