import json
from datetime import timedelta, datetime
from typing import Type

from dijkstar import Graph, find_path

# The cost function is passed the current node (u), a neighbor (v) of the current node,
# the edge that connects u to v, and the edge that was traversed previously to get to the current node.
# A cost function is most useful when computing costs dynamically. If costs in your graph are fixed, a cost function
# will only add unnecessary overhead. In the example above, a penalty is added when the street name changes.
#
# When using a cost function, one recommendation is to compute a base cost when possible.
# For example, for a graph that represents a street network, the base cost for each street segment (edge)
# could be the length of the segment multiplied by the speed limit. There are two advantages to this:
# the size of the graph will be smaller and the cost function will be doing less work, which may improve performance.
# def cost_func(u, v, edge, prev_edge):
#     length, name = edge
#     if prev_edge:
#         prev_name = prev_edge[1]
#     else:
#         prev_name = None
#     cost = length
#     if name != prev_name:
#         cost += 10
#     return cost
from fibheap import makefheap, fheappush, fheappop
from sqlalchemy.engine import result

from database.db_models import StopTimes, Transfers, session, Stops

previous_stop_sequence = None
previous_from_stop = None
previous_stop_dept_time = None
# current_trip_id = None


def build_graph():
    # {u: {v: e, ...}, ...}  # Node v is a adjacent to u via edge e
    graph = Graph()

    all_stop_times = (session.query(StopTimes).join(Stops.stop_names).order_by(StopTimes.trip_id.desc()).order_by(StopTimes.stop_sequence.asc())
        .all()
                      )    # TODO: each stop is doubled, I don't know why
    # all_stop_ids_with_transfers =  (StopTimes.query.with_entities(StopTimes.trip_id, StopTimes.arrival_time, StopTimes.departure_time, StopTimes.stop_id, StopTimes.stop_sequence)
    #     .join(Transfers.from_stop_id)
    #     .distinct()
    #                   )
    # all_transfer_times = session.query(Transfers).join(Stops.stop_names).all()    # this is working
    all_transfer_times = session.query(Transfers).all()

    # Classic connection duration
    for row in all_stop_times:
        # TODO: this may not be the most effective way
        global previous_stop_sequence
        previous_stop_sequence = row.stop_sequence

        global previous_from_stop

        global previous_stop_dept_time

        if row.stop_sequence == 1:
            previous_stop_dept_time = _get_timedelta(row.departure_time)
            previous_from_stop = row.stops.stop_name
            graph.add_node(previous_from_stop, neighbors={"neighbour2": "value2"})
            continue

        to_stop = row.stops.stop_name

        current_stop_arr_time = _get_timedelta(row.arrival_time)
        travel_duration = (current_stop_arr_time - previous_stop_dept_time).seconds

        # TODO: save trip_id to each edge?

        graph.add_node(to_stop, neighbors={"neighbout1": "value"})
        graph.add_edge(previous_from_stop, to_stop, travel_duration)

        previous_stop_dept_time = _get_timedelta(row.departure_time)
        previous_from_stop = row.stops.stop_name
    print(graph)

    # # Include transfers
    for row in all_transfer_times:
        # graph.add_node(row.stops.stop_name)
        # graph.add_edge(row.stops.stop_name, row.to_stop_id, row.min_transfer_time)
        from_stop = session.query(Stops).filter(Stops.stop_id == row.from_stop_id).one()
        to_stop = session.query(Stops).filter(Stops.stop_id == row.to_stop_id).one()
        # graph.add_node(from_stop.stop_name)
        # graph.add_node(to_stop.stop_name)
        graph.add_edge(from_stop.stop_name, to_stop.stop_name, row.min_transfer_time)
    print(graph)
    graph.dump("test_graph")

    # # Include waiting times
    # for row in all_stop_ids_with_transfers:
    #     # Use casy:
    #     # 1. prijedu na Moran, vystoupim, cekam na Morani, nez prijede linka, na kterou chci prestoupit
    #         # vem arrival_time linky do Morane a departure_time druhe linky z Morane z casove nejblizsiho tripu k tomu prvnimu
    #     # 2. prijedu na Florenc B, prestoupim na Florenc C, cekam na Florenc C, nez prijede linka
    #         # vem arrival_time linky do Florenc B, pripocti min. transfer time a spocitej to az do departure_time druhe linky z Florenc C z casove nejblizsiho tripu k tomu prvnimu
    #     graph.add_edge(previous_from_stop, to_stop, travel_duration)


    # from_stop_id, to_stop_id, transfer_type, min_transfer_time, from_trip_id, to_trip_id
    # U1072Z101P, U1072Z121P, 2, 240,,
    # U1072Z121P, U1072Z101P, 2, 240,,
    #
    # trip_id, arrival_time, departure_time, stop_id, stop_sequence, stop_headsign, pickup_type, drop_off_type, shape_dist_traveled
    # 991_1412_181225, 12: 17:40, 12: 17:40, U1071Z102P, 1,, 0, 0, 0.00000
    # 991_1412_181225, 12: 19:40, 12: 20:10, U953Z102P, 2,, 0, 0, 1.15243
    # 991_1412_181225, 12: 22:25, 12: 22:55, U713Z102P, 3,, 0, 0, 2.52832
    # 991_1412_181225, 12: 24:35, 12: 25:05, U921Z102P, 4,, 0, 0, 3.76197
    # 991_1412_181225, 12: 26:30, 12: 26:50, U118Z102P, 5,, 0, 0, 4.69861
    #
    # graph.add_edge(1, 2, 110)
    # graph.add_edge(2, 3, 125)
    # graph.add_edge(3, 4, 108)
    # data = Graph.load("test_graph")
    # print(data)

# TODO: needs to cover also "via" stops and other stuff
def find_shortest_path(graph_file: str, from_stop: str, to_stop: str):
    data = Graph.load(graph_file)
    #Also accepts an optional heuristic function that is used to push the algorithm toward a destination
    # instead of fanning out in every direction. Using such a heuristic function converts Dijkstra to A*.
    found_path = find_path(data, from_stop, to_stop, heuristic_func=None, cost_func=None)
    return found_path

def add_weights():
    # https://stackoverflow.com/questions/483488/strategy-to-find-your-best-route-via-public-transportation-only
    # One key to a sucessfull algorith was using a path cost function based on travel and waiting time
    # multiplied by diffrent weightes. Known in Swedish as “kresu"-time these weighted times reflect the fact that,
    # for example, one minute’s waiting time is typically equivalent in “inconvenience” to two minutes
    # of travelling time.
    # x1 - Travel time
# x2 - Walking between stops
# x2 - Waiting at a stop during the journey. Stops under roof, with shops, etc can get a slightly lower weight and crowded stations a higher to tune the algorithm.
# The weight for the waiting time at the first stop is a function of trafic intensity and can be between 0.5 to 3.
    return []

# def heurist(u, v, edge, prev_edge):
#     length, name = edge
#     if prev_edge:
#         prev_name = prev_edge[1]
#     else:
#         prev_name = None
#     cost = length
#     if name != prev_name:
#         cost += 10
#     return cost


def _get_timedelta(stop_time: str) -> timedelta:
    stop_timepoint = datetime.strptime(stop_time, "%H:%M:%S").time()
    stop_timedelta = timedelta(hours=stop_timepoint.hour, minutes=stop_timepoint.minute,
                                        seconds=stop_timepoint.second)
    return stop_timedelta


if __name__ == "__main__":
    build_graph()
    found_path = find_shortest_path("test_graph", "U50S1", "U157S1")
    print(f"PATH: {found_path}")
