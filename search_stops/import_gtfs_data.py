import os
import datetime

import pytz
from gtfspy import import_gtfs
from gtfspy import gtfs
from gtfspy import osm_transfers
from gtfspy.osm_transfers import add_walk_distances_to_db_python
from pytz import utc

from search_stops.config import SQLALCHEMY_DATABASE_URI


def load_or_import_example_gtfs(verbose=False):
    imported_database_path = "../database/gtfs_data.db"
    # import_gtfs.import_gtfs(["../data/traffic_source.zip"],
    #                         imported_database_path,  # output: where to create the new sqlite3 database
    #                         print_progress=verbose,
    #                         location_name="Prague")

    # add_distances = add_walk_distances_to_db_python("../database/gtfs_data.db", "../data/Prag.osm.pbf", cutoff_distance_m=1000)

    # Not this is an optional step, which is not necessary for many things.
    print("Computing walking paths using OSM")
    G = gtfs.GTFS(imported_database_path)
    now = datetime.datetime.now(tz=pytz.timezone("Europe/Prague")).strftime("%H:%M")
    G.meta['data_updated'] = now

    osm_path = f"../data/Prag.osm.pbf"

    # when using with the Kuopio test data set,
    # this should raise a warning due to no nearby OSM nodes for one of the stops.

    # FIXME: skip this for now. The problem is at node_lats = networkx.get_node_attributes(walk_network[0], 'lat'),
    #  when it fails on File "C:\Users\Misa\PycharmProjects\transportation\venv\lib\site-packages\gtfspy\osm_transfers.py", line 166, in create_walk_network_from_osm
    #     data["distance"] = wgs84_distance(node_lats[source],
    #   KeyError: 703192
    #  because there are no "lat" and "lon" keys in the dictionary. Either the graph is created wrongly or the osm file is the
    #  wrong one or the file on Finnish transport looks differently.
    # osm_transfers.add_walk_distances_to_db_python(imported_database_path, osm_path)

    # print("Note: for large cities we have also a faster option for computing footpaths that uses Java.)")
    # dir_path = os.path.dirname(os.path.realpath(__file__))
    # java_path = os.path.join(dir_path, "../java_routing/")
    # print("Please see the contents of " + java_path + " for more details.")

    # Now you can access the imported database using a GTFS-object as an interface:
    G = gtfs.GTFS(imported_database_path)

    if verbose:
        print("Location name:" + G.get_location_name())  # should print Kuopio
        print("Time span of the data in unixtime: " + str(G.get_approximate_schedule_time_span_in_ut()))
        # prints the time span in unix time
    return G


def show_map():
    from gtfspy import mapviz
    # from example_import import load_or_import_example_gtfs
    import matplotlib.pyplot as plt

    g = load_or_import_example_gtfs()
    # g is now a gtfspy.gtfs.GTFS object

    # Plot the route network and all stops to the same axes
    ax = mapviz.plot_route_network_from_gtfs(g, scalebar=True)
    mapviz.plot_all_stops(g, ax)

    # Plot also a thumbnail figure highlighting the central areas:
    # ax_thumbnail = mapviz.plot_route_network_thumbnail(g)

    # ax_thumbnail.figure.savefig("test_thumbnail.jpg")

    plt.show()


if __name__ == "__main__":
    load_or_import_example_gtfs(verbose=True)
    show_map()