from search_stops.dijkstra import build_graph, find_shortest_path


class TestSearchingAlgorithm:
    def test_build_graph(
        self,
        fill_stops_table,
        fill_trips_related_tables,
        fill_transfers_table
    ):
        graph = build_graph()

        found_path = find_shortest_path("test_graph", "Budějovická", "Spořilov")
        print(f"PATH: {found_path}")

        # assert graph == "blah"