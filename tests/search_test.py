from datetime import time

import pytest

from database.db_models import StopsVirtualSchema, session
from search_stops.search import (
    match_stop,
    _filter_stop_ids,
    _filter_trip_ids,
    _filter_arrival_trip_ids_for_direct_trips,
)


class TestStopSearch:
    @pytest.mark.parametrize(
        "stop, expected",
        [
            ("Budějovická", [{"stop_id": "U50S1", "stop_name": "Budějovická"}]),
            ("Budejovicka", [{"stop_id": "U50S1", "stop_name": "Budějovická"}]),
            ("Budej", [{"stop_id": "U50S1", "stop_name": "Budějovická"}]),
            (("Buda", [])),
        ],
    )
    def test_match_stop(self, stop, expected, fill_stops_table):
        matched_stops = match_stop(stop)
        dumped_stops = StopsVirtualSchema(many=True, only=["stop_id", "stop_name"]).dump(
            matched_stops
        )
        assert dumped_stops == expected


class TestFilterStopIds:
    def test_filter_stop_ids(self, fill_stops_table):
        found_stop_ids = _filter_stop_ids("Budějovická")
        query_stop_ids = session.execute(found_stop_ids).fetchall()
        assert query_stop_ids == [("U50S1",), ("U50Z2P",)]


class TestFilterArrivalTripIdsForDirectTrips:
    def test_filter_trip_ids(self, fill_stops_table, fill_trips_related_tables):
        filter_trip_ids_query = _filter_arrival_trip_ids_for_direct_trips(
            _filter_stop_ids("Spořilov")
        )
        trip_ids = session.execute(filter_trip_ids_query).fetchall()
        assert trip_ids == [("170_178_190401",), ("170_178_190402",)]


class TestFilterTripIds:
    @pytest.mark.parametrize(
        "travel_day, departure_time, arrival_time, expected",
        [
            (
                1,
                time(14, 30),
                None,
                [
                    (
                        4,
                        "170_178_190402",
                        "14:35:00",
                        "14:35:00",
                        "U50S1",
                        5,
                        None,
                        0,
                        "0",
                        "0.00000",
                    ),
                    (
                        7,
                        "993_2605_190831",
                        "14:34:00",
                        "14:34:00",
                        "U50Z2P",
                        3,
                        None,
                        0,
                        "0",
                        "1.24604",
                    ),
                ],
            ),
            (1, time(18, 30), None, []),
            (
                1,
                time(14, 30),
                time(15, 30),
                [
                    (
                        1,
                        "170_178_190401",
                        "14:30:00",
                        "14:30:00",
                        "U50S1",
                        1,
                        None,
                        0,
                        "0",
                        "0.00000",
                    ),
                    (
                        4,
                        "170_178_190402",
                        "14:35:00",
                        "14:35:00",
                        "U50S1",
                        5,
                        None,
                        0,
                        "0",
                        "0.00000",
                    ),
                    (
                        7,
                        "993_2605_190831",
                        "14:34:00",
                        "14:34:00",
                        "U50Z2P",
                        3,
                        None,
                        0,
                        "0",
                        "1.24604",
                    ),
                ],
            ),
            (1, time(14, 30), time(9, 30), []),
            (
                5,
                time(14, 30),
                None,
                [
                    (
                        4,
                        "170_178_190402",
                        "14:35:00",
                        "14:35:00",
                        "U50S1",
                        5,
                        None,
                        0,
                        "0",
                        "0.00000",
                    ),
                    (
                        7,
                        "993_2605_190831",
                        "14:34:00",
                        "14:34:00",
                        "U50Z2P",
                        3,
                        None,
                        0,
                        "0",
                        "1.24604",
                    ),
                ],
            ),
            (
                6,
                time(14, 30),
                None,
                [
                    (
                        4,
                        "170_178_190402",
                        "14:35:00",
                        "14:35:00",
                        "U50S1",
                        5,
                        None,
                        0,
                        "0",
                        "0.00000",
                    )
                ],
            ),
        ],
    )
    def test_filter_trip_ids(
        self,
        travel_day,
        departure_time,
        arrival_time,
        expected,
        fill_stops_table,
        fill_trips_related_tables,
    ):
        filter_trip_ids_query = _filter_trip_ids(
            _filter_stop_ids("Budějovická"), travel_day, departure_time, arrival_time
        )
        trip_ids = session.execute(filter_trip_ids_query).fetchall()
        assert trip_ids == expected


class TestTripSearch:
    @pytest.mark.parametrize(
        "departure_stop_name, arrival_stop_name, travel_date, arrival_time, mode, transfer_counts, stop_included, departure_time, matched_trips, first_and_last_trip",
        [
            (  # Budejovicka -> Sporilov, operating at the day and time requested, direct trip, arrival time set
                # "U50S1",
                # "U697Z1P",
                "Budějovická",
                "Spořilov",
                "20191214",
                "14:40:40",
                2,
                7,
                None,
                None,
                2,
                [
                    {
                        "arrival_time": "14:34:00",
                        "departure_time": "14:34:00",
                        "drop_off_type": 0,
                        "id": 3,
                        "pickup_type": None,
                        "shape_dist_traveled": "1.24604",
                        "stop_headsign": "0",
                        "stop_id": "U697Z1P",
                        "stop_sequence": 3,
                        "trip_id": "170_178_190401",
                        "trips": [1],
                    },
                    {
                        "arrival_time": "14:39:00",
                        "departure_time": "14:39:00",
                        "drop_off_type": 0,
                        "id": 5,
                        "pickup_type": None,
                        "shape_dist_traveled": "1.24604",
                        "stop_headsign": "0",
                        "stop_id": "U697Z1P",
                        "stop_sequence": 3,
                        "trip_id": "170_178_190402",
                        "trips": [2],
                    },
                ],
            ),
            (  # Budejovicka -> Borislavka, operating at the day and time requested, one-time transfer trip, arrival time set
                # "U50Z2P",
                # "U157S1",
                "Budějovická",
                "Bořislavka",
                "2019-12-16",
                "14:40:40",
                2,
                7,
                None,
                None,
                0,
                [],
            ),
            (  # Budejovicka -> Sporilov, operating at the day and time requested, direct trip, departure time set
                # "U50S1",
                # "U697Z1P",
                "Budějovická",
                "Spořilov",
                "20191214",
                None,
                2,
                7,
                None,
                "14:33:40",
                1,
                [
                    {
                        "arrival_time": "14:34:00",
                        "departure_time": "14:34:00",
                        "drop_off_type": 0,
                        "id": 3,
                        "pickup_type": None,
                        "shape_dist_traveled": "1.24604",
                        "stop_headsign": "0",
                        "stop_id": "U697Z1P",
                        "stop_sequence": 3,
                        "trip_id": "170_178_190401",
                        "trips": [1],
                    },
                    {
                        "arrival_time": "14:39:00",
                        "departure_time": "14:39:00",
                        "drop_off_type": 0,
                        "id": 5,
                        "pickup_type": None,
                        "shape_dist_traveled": "1.24604",
                        "stop_headsign": "0",
                        "stop_id": "U697Z1P",
                        "stop_sequence": 3,
                        "trip_id": "170_178_190402",
                        "trips": [2],
                    },
                ],
            ),
            (  # Budejovicka -> Borislavka, not operating at the day and time requested
                # "U50Z2P",
                # "U157S1",
                "Budějovická",
                "Bořislavka",
                "20191214",
                "15:20:40",
                2,
                7,
                None,
                None,
                0,
                [],
            ),
        ],
    )
    # TODO: pathways not included yet, transfer counts too, stop included too,
    def test_search_connection(
        self,
        departure_stop_name,
        arrival_stop_name,
        travel_date,
        arrival_time,
        mode,
        transfer_counts,
        stop_included,
        departure_time,
        matched_trips,
        first_and_last_trip,
        fill_trips_related_tables,
        fill_stops_table,
        client,
    ):
        user_input_response = client.post(
            path="/search_connection",
            data={
                "departure_stop_name": departure_stop_name,
                "arrival_stop_name": arrival_stop_name,
                "stop_included": stop_included,
                "mode": mode,
                "transfer_counts": transfer_counts,
                "travel_date": travel_date,
                "arrival_time": arrival_time,
                "departure_time": departure_time,
            },
        )

        assert len(user_input_response.json) == matched_trips

        if matched_trips:
            user_input_response.json[0][-1], user_input_response.json[-1][-1] = first_and_last_trip
        else:
            assert user_input_response.json == first_and_last_trip
