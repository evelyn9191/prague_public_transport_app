from contextlib import ContextDecorator, closing

import pytest

# from sqlalchemy import create_engine
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker

# from sqlalchemy_utils import database_exists, create_database, drop_database
#
from sqlalchemy_utils import database_exists, create_database

from database import db_models

# from search_stops import config as config_file
from database.db_models import Stops, StopsVirtual, StopTimes, Trips, Base, session, Transfers
from search_stops import config as config_file
from search_stops.config import SQLALCHEMY_DATABASE_URI


@pytest.fixture(scope="session")
def db_engine():
    engine = create_engine(SQLALCHEMY_DATABASE_URI)
    print(f"ENGINE {engine}")
    if not database_exists(engine.url):
        create_database(engine.url)

    yield engine

    # drop_database(engine.url)


@pytest.fixture
def create_tables():  # TODO: it should be created against database_uri
    """Test database."""
    engine = create_engine(SQLALCHEMY_DATABASE_URI)

    Base.metadata.drop_all(engine)

    with closing(sessionmaker(bind=engine)()) as session:
        db_models.create_tables(engine)
        session.commit()
    yield

    engine.dispose()


@pytest.fixture
def fill_stops_table(create_tables):
    models_to_fill = [
        (Stops, STOPS),
        (StopsVirtual, STOPS_SIMPLE),
    ]  # TODO: the automatic filling of stops_virtual does not work in the reality, need to be fixed
    for model, data in models_to_fill:
        stops = (model(**entry) for entry in data)
        session.add_all(stops)
        session.commit()


@pytest.fixture
def fill_trips_related_tables(create_tables):
    models_to_fill = [(Trips, TRIPS), (StopTimes, STOP_TIMES)]
    for model, data in models_to_fill:
        trips = (model(**entry) for entry in data)
        session.add_all(trips)
        session.commit()


@pytest.fixture
def fill_transfers_table(create_tables):
    models_to_fill = [(Transfers, TRANSFERS), (StopTimes, STOP_TIMES)]
    for model, data in models_to_fill:
        trips = (model(**entry) for entry in data)
        session.add_all(trips)
        session.commit()

STOPS = [
    {
        "stop_id": "U50S1",
        "stop_name": "Budějovická",
        "stop_lat": 50.04441,
        "stop_lon": 14.44879,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 1,
        "parent_station": "",
        "wheelchair_boarding": 1,
        "level_id": None,
        "platform_code": "",
    },
    {
        "stop_id": "U643Z1P",
        "stop_name": "Severozápadní",
        "stop_lat": 50.04441,
        "stop_lon": 14.44879,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 1,
        "parent_station": "",
        "wheelchair_boarding": 1,
        "level_id": None,
        "platform_code": "",
    },
    {
        "stop_id": "U157S1",
        "stop_name": "Bořislavka",
        "stop_lat": 50.09834,
        "stop_lon": 14.36283,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 1,
        "parent_station": "",
        "wheelchair_boarding": 1,
        "level_id": None,
        "platform_code": "",
    },
    {
        "stop_id": "U50Z2P",
        "stop_name": "Budějovická",
        "stop_lat": 50.04484,
        "stop_lon": 14.44789,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 0,
        "parent_station": "",
        "wheelchair_boarding": 0,
        "level_id": None,
        "platform_code": "B",
    },
    {
        "stop_id": "U697Z1P",
        "stop_name": "Spořilov",
        "stop_lat": 50.05094,
        "stop_lon": 14.48221,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 0,
        "parent_station": "",
        "wheelchair_boarding": 1,
        "level_id": None,
        "platform_code": "A",
    },
    {
        "stop_id": "U400Z3P",
        "stop_name": "Muzeum C",
        "stop_lat": 50.05094,
        "stop_lon": 14.48221,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 0,
        "parent_station": "",
        "wheelchair_boarding": 1,
        "level_id": None,
        "platform_code": "A",
    },
    {
        "stop_id": "U1071Z101P",
        "stop_name": "Depo Hostivař",
        "stop_lat": 50.04441,
        "stop_lon": 14.44879,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 1,
        "parent_station": "",
        "wheelchair_boarding": 1,
        "level_id": None,
        "platform_code": "",
    },
    {
        "stop_id": "U400Z1P",
        "stop_name": "Muzeum A",
        "stop_lat": 50.05094,
        "stop_lon": 14.48221,
        "zone_id": "P",
        "stop_url": "",
        "location_type": 0,
        "parent_station": "",
        "wheelchair_boarding": 1,
        "level_id": None,
        "platform_code": "A",
    },
]

STOPS_SIMPLE = [
    {  # Budejovicka na trase Budejovicka -> Sporilov
        "id": 1,  # TODO: delete primary keys from the dict
        "stop_id": "U50S1",
        "stop_name": "Budějovická",
    },
    {"id": 2, "stop_id": "U643Z1P", "stop_name": "Severozápadní"},
    {"id": 3, "stop_id": "U697Z1P", "stop_name": "Spořilov"},
    {  # Budejovicka na trase Budejovicka -> Muzeum
        "id": 4,
        "stop_id": "U50Z2P",
        "stop_name": "Budějovická",
    },
    {"id": 5, "stop_id": "U400Z1P", "stop_name": "Muzeum"},  # Muzeum A
    {"id": 6, "stop_id": "U400Z3P", "stop_name": "Muzeum"},  # Muzeum C
    {"id": 7, "stop_id": "U157S1", "stop_name": "Bořislavka"},
]

TRIPS = [
    {  # bus Budejovicka -> Sporilov
        "route_id": "L170",
        "service_id": "1111111-1",
        "trip_id": "170_178_190401",
        "trip_headsign": "Jižní Město",
        "trip_short_name": None,
        "shape_id": 1,
        "wheelchair_accessible": None,
        "block_id": "L170V1",
        "direction_id": 1,
        "bikes_allowed": 2,
        "exceptional": 0,
        "trip_operation_type": 1,
    },
    {  # bus Budejovicka -> Sporilov, trip no. 2
        "route_id": "L170",
        "service_id": "1111111-1",
        "trip_id": "170_178_190402",
        "trip_headsign": "Jižní Město",
        "trip_short_name": None,
        "shape_id": 1,
        "wheelchair_accessible": None,
        "block_id": "L170V1",
        "direction_id": 1,
        "bikes_allowed": 2,
        "exceptional": 0,
        "trip_operation_type": 1,
    },
    {  # metro Haje -> Letnany
        "route_id": "L993",
        "service_id": "1111100-1",
        "trip_id": "993_2605_190831",
        "trip_headsign": "Háje",
        "trip_short_name": None,
        "shape_id": 1,
        "wheelchair_accessible": None,
        "block_id": "L170V1",
        "direction_id": 1,
        "bikes_allowed": 2,
        "exceptional": 0,
        "trip_operation_type": 1,
    },
    {  # metro Depo Hostivar -> Nemocnice Motol
        "route_id": "L991",
        "service_id": "1111100-1",
        "trip_id": "991_529_181224",
        "trip_headsign": "Depo Hostivař",
        "trip_short_name": None,
        "shape_id": 1,
        "wheelchair_accessible": None,
        "block_id": "L170V1",
        "direction_id": 1,
        "bikes_allowed": 2,
        "exceptional": 0,
        "trip_operation_type": 1,
    },
]

STOP_TIMES = [
    {  # Budejovicka na trase Budejovicka -> Sporilov
        "trip_id": "170_178_190401",
        "arrival_time": "14:30:00",
        "departure_time": "14:30:00",
        "stop_id": "U50S1",
        "stop_sequence": 1,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "0.00000",
    },
    {  # Severozapadni
        "trip_id": "170_178_190401",
        "arrival_time": "14:32:00",
        "departure_time": "14:32:00",
        "stop_id": "U643Z1P",
        "stop_sequence": 2,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "0.93549",
    },
    {  # Sporilov
        "trip_id": "170_178_190401",
        "arrival_time": "14:34:00",
        "departure_time": "14:34:00",
        "stop_id": "U697Z1P",
        "stop_sequence": 3,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "1.24604",
    },
    {  # Budejovicka na trase Budejovicka -> Sporilov, trip no. 2
        "trip_id": "170_178_190402",
        "arrival_time": "14:35:00",
        "departure_time": "14:35:00",
        "stop_id": "U50S1",
        "stop_sequence": 1,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "0.00000",
    },
    {  # Severozapadni
        "trip_id": "170_178_190402",
        "arrival_time": "14:36:00",
        "departure_time": "14:36:00",
        "stop_id": "U643Z1P",
        "stop_sequence": 6,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "0.93549",
    },
    {  # Sporilov, trip no. 2
        "trip_id": "170_178_190402",
        "arrival_time": "14:39:00",
        "departure_time": "14:39:00",
        "stop_id": "U697Z1P",
        "stop_sequence": 7,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "1.24604",
    },
    {  # Budejovicka na trase Budejovicka -> Muzeum C
        "trip_id": "993_2605_190831",
        "arrival_time": "14:34:00",
        "departure_time": "14:34:00",
        "stop_id": "U50Z2P",
        "stop_sequence": 1,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "1.24604",
    },
    {  # Muzeum metro C
        "trip_id": "993_2605_190831",
        "arrival_time": "14:40:00",
        "departure_time": "14:40:00",
        "stop_id": "U400Z3P",
        "stop_sequence": 7,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "1.24604",
    },
    {  # Depo Hostivař metro A
        "trip_id": "991_529_181224",
        "arrival_time": "14:30:00",
        "departure_time": "14:30:20",
        "stop_id": "U1071Z101P",
        "stop_sequence": 1,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "1.24604",
    },
    {  # Muzeum metro A
        "trip_id": "991_529_181224",
        "arrival_time": "14:45:00",
        "departure_time": "14:45:20",
        "stop_id": "U400Z1P",
        "stop_sequence": 8,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "1.24604",
    },
    {  # Borislavka na trase Muzeum A -> Borislavka
        "trip_id": "991_529_181224",
        "arrival_time": "14:58:00",
        "departure_time": "14:58:30",
        "stop_id": "U157S1",
        "stop_sequence": 13,
        "pickup_type": None,
        "drop_off_type": 0,
        "stop_headsign": 0,
        "shape_dist_traveled": "1.24604",
    },
]

TRANSFERS = [
                {  # Transfer Muzeum C -> Muzeum A
                    "from_stop_id": "U400Z3P",
                    "to_stop_id": "U400Z1P",
                    "transfer_type": 2,
                    "min_transfer_time": 240,
                    "from_trip_id": None,
                    "to_trip_id": None,
                },
]

class override_config(ContextDecorator):
    def __init__(self, **config_kwargs):
        self.config_kwargs = config_kwargs
        self.old_values = {}

    def __enter__(self):
        for variable, new_value in self.config_kwargs.items():
            if variable not in config_file.__dict__:
                continue
            self.old_values[variable] = getattr(config_file, variable)
            setattr(config_file, variable, new_value)

    def __exit__(self, exc_type, exc_val, exc_tb):
        for variable, old_value in self.old_values.items():
            setattr(config_file, variable, old_value)


def pytest_assertrepr_compare(op, left, right):
    if op in ("==", "!="):
        return ["{0} {1} {2}".format(left, op, right)]
