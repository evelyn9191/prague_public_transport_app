import socket

from transportation.db.update_db import update_tables

REMOTE_SERVER = "www.google.com"


def update_database():
    connection = is_connected(REMOTE_SERVER)
    if connection:
        update_tables()


def is_connected(hostname):
    try:
        # see if we can resolve the host name -- tells us if there is
        # a DNS listening
        host = socket.gethostbyname(hostname)
        # connect to the host -- tells us if the host is actually
        # reachable
        s = socket.create_connection((host, 80), 2)
        s.close()
        return True
    except Exception:
        pass
    return False
