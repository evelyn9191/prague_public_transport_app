import glob
import io
import zipfile
from datetime import datetime
from pathlib import Path
from typing import Optional, Type

import pandas as pd
import requests

from database.db_models import (
    DatabaseUpdate,
    Agency,
    Calendar,
    CalendarDates,
    Routes,
    Shapes,
    StopTimes,
    AGENCY,
    CALENDAR,
    CALENDAR_DATES,
    ROUTES,
    SHAPES,
    STOP_TIMES,
    STOPS,
    Stops,
    TRANSFERS,
    Transfers,
    TRIPS,
    Trips,
    db,
)
from search_stops.config import TRAFFIC_SOURCE_ZIP


def update_tables() -> None:
    # traffic_files = download_traffic_source()
    #
    # if not traffic_files:
    #     return None

    # with tempfile.TemporaryDirectory() as tmpdir:
    # traffic_files.extractall()

    # for txt_file in tmpdir:
    for txt_file in glob.glob(
        "C://Users/Misa/PycharmProjects/transportation/data/time_tables/*.txt"
    ):
        insert_all(txt_file)


def download_traffic_source() -> Optional[zipfile.ZipFile]:
    last_update = (
        db.session.query(DatabaseUpdate.last_update_attempt)
        .filter(DatabaseUpdate.successful is True)
        .first()
    )
    if last_update and datetime.utcnow() - last_update < 10:
        return None

    traffic_file = requests.get(TRAFFIC_SOURCE_ZIP)
    traffic_contents = zipfile.ZipFile(io.BytesIO(traffic_file.content))
    return traffic_contents


def insert_all(txt_file: str) -> None:
    with open(txt_file, encoding="utf-8") as f:
        df = pd.read_csv(f)

    filename = Path(txt_file).stem
    schema = _get_schema(filename)

    try:
        db.session.bulk_insert_mappings(schema, df.to_dict())
        # TODO: here call insert on stops_virtual
        db.session.close()
    except Exception as e:
        return _track_last_update_attempt(False, filename, str(e))
    _track_last_update_attempt(True, filename)


def _track_last_update_attempt(success: bool, table_name: str, exception: Optional[str] = None):
    data = {
        "successful": success,
        "last_update_attempt": datetime.utcnow(),
        "table_name": table_name,
        "exception": exception,
    }

    db.session.add(DatabaseUpdate(**data))
    db.session.commit()


def _get_schema(tablename: str) -> Type[db.Model]:
    return {
        AGENCY: Agency,
        CALENDAR: Calendar,
        CALENDAR_DATES: CalendarDates,
        ROUTES: Routes,
        SHAPES: Shapes,
        STOP_TIMES: StopTimes,
        STOPS: Stops,
        TRANSFERS: Transfers,
        TRIPS: Trips,
    }.get(tablename)
