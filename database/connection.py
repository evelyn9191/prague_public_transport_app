import sqlite3


def create_connection(SQLALCHEMY_DATABASE_URI):
    conn = None
    try:
        conn = sqlite3.connect(SQLALCHEMY_DATABASE_URI)
    except sqlite3.Error as e:
        print(e)
    finally:
        if conn:
            conn.close()
