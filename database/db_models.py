from marshmallow import EXCLUDE
from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy import Integer, Column, Date, String, Boolean, DateTime, Enum, ForeignKey, orm
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import sqlalchemy as sa

from search_stops.config import SQLALCHEMY_DATABASE_URI

AGENCY, CALENDAR, CALENDAR_DATES, ROUTES, SHAPES, STOP_TIMES, STOPS, TRANSFERS, TRIPS = (
    "agency",
    "calendar",
    "calendar_dates",
    "routes",
    "shapes",
    "stop_times",
    "stops",
    "transfers",
    "trips",
)

Base = declarative_base()
engine = sa.create_engine(SQLALCHEMY_DATABASE_URI)
Base.metadata.bind = engine
session = orm.scoped_session(orm.sessionmaker())(bind=engine)


class Agency(Base):
    __tablename__ = "agency"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    agency_id = Column(Integer)
    agency_name = Column(String)
    agency_url = Column(String)
    agency_timezone = Column(String)
    agency_lang = Column(String)
    agency_phone = Column(String)

    def __repr__(self):
        return f"<{self.__class__.__name__}(agency_name={self.agency_name}, agency_id={self.agency_id})>"


class Calendar(Base):
    __tablename__ = "calendar"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    service_id = Column(Integer)
    monday = Column(Integer)
    tuesday = Column(Integer)
    wednesday = Column(Integer)
    thursday = Column(Integer)
    friday = Column(Integer)
    saturday = Column(Integer)
    sunday = Column(Integer)
    start_date = Column(Date)
    end_date = Column(Date)

    def __repr__(self):
        return f"<{self.__class__.__name__}(service_id={self.service_id})>"


class CalendarDates(Base):
    __tablename__ = "calendar_dates"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    service_id = Column(Integer)
    date = Column(Date)
    exception_type = Column(Integer)

    def __repr__(self):
        return f"<{self.__class__.__name__}(service_id={self.service_id}, exception={self.exception_type})>"


class Routes(Base):
    __tablename__ = "routes"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    route_id = Column(Integer)
    agency_id = Column(Integer)
    route_short_name = Column(String)
    route_long_name = Column(String)
    route_type = Column(Integer)
    route_color = Column(String)
    route_text_color = Column(String)

    def __repr__(self):
        return f"<{self.__class__.__name__}(route_short_name={self.route_short_name}, route_long_name={self.route_long_name})>"


class Shapes(Base):
    __tablename__ = "shapes"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    shape_id = Column(Integer)
    shape_pt_lat = Column(String)
    shape_pt_lon = Column(String)
    shape_pt_sequence = Column(Integer)

    def __repr__(self):
        return f"<{self.__class__.__name__}(shape_id={self.shape_id}, shape_pts={self.shape_pt_lat, self.shape_pt_lon})>"


class Trips(Base):
    __tablename__ = "trips"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    route_id = Column(String)
    service_id = Column(String)
    trip_id = Column(String, ForeignKey("stop_times.trip_id"))
    trip_headsign = Column(String)
    trip_short_name = Column(String)
    shape_id = Column(String)
    wheelchair_accessible = Column(Integer)
    block_id = Column(Integer)
    direction_id = Column(Integer)
    bikes_allowed = Column(Integer)
    exceptional = Column(Integer)
    trip_operation_type = Column(Integer)

    def __repr__(self):
        return f"<{self.__class__.__name__}(trip_id={self.trip_id}, trip_headsign={self.trip_headsign})>"


class StopsVirtual(Base):
    __tablename__ = "stops_virtual"

    # TODO: primary_key is not being created
    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    stop_id = Column(String)
    stop_name = Column(String, ForeignKey("stops.stop_name"))

    def __repr__(self):
        return f"<{self.__class__.__name__}Virtual table Stops: (stop_name={self.stop_id})>"


class StopsVirtualSchema(ModelSchema):

    class Meta:
        unknown = EXCLUDE
        include_fk = True
        model = StopsVirtual
        sqla_session = session

class Stops(Base):
    __tablename__ = "stops"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    stop_id = Column(String)
    stop_name = Column(String)
    stop_lat = Column(String)
    stop_lon = Column(String)
    zone_id = Column(String)
    stop_url = Column(String)
    location_type = Column(Integer)
    parent_station = Column(String)
    wheelchair_boarding = Column(Integer)
    level_id = Column(Integer)
    platform_code = Column(String)

    indexed_stop_names = relationship("StopsVirtual", backref="stops")
    stop_names = relationship("StopTimes", backref="stops")

    def __repr__(self):
        return f"<{self.__class__.__name__}(stop_id={self.stop_id}, stop_name={self.stop_name})>"

class Transfers(Base):
    __tablename__ = "transfers"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    from_stop_id = Column(String, ForeignKey("stop_times.stop_id", ondelete="CASCADE"), nullable=False)
    to_stop_id = Column(String)
    transfer_type = Column(Integer)
    min_transfer_time = Column(Integer)
    from_trip_id = Column(String)
    to_trip_id = Column(String)

    stop_names = relationship("Stops", foreign_keys=[from_stop_id], primaryjoin='Stops.stop_id == Transfers.from_stop_id')

    def __repr__(self):
        return f"<{self.__class__.__name__}(from_stop_id={self.from_stop_id}, to_stop_name={self.to_stop_id})>"

class StopTimes(Base):
    __tablename__ = "stop_times"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    trip_id = Column(String)
    arrival_time = Column(String)
    departure_time = Column(String)
    stop_id = Column(String, ForeignKey("stops.stop_id", ondelete="CASCADE"))
    stop_sequence = Column(Integer)
    pickup_type = Column(Integer)
    drop_off_type = Column(Integer)
    stop_headsign = Column(String)
    shape_dist_traveled = Column(String)

    trips = relationship("Trips", backref="stop_times")
    transfer_stops = relationship(Transfers, backref="stop_times")

    def __repr__(self):
        return f"<{self.__class__.__name__}(trip_id={self.trip_id}, stop_id={self.stop_id})>"


class StopTimesSchema(ModelSchema):
    class Meta:
        unknown = EXCLUDE
        include_fk = True
        model = StopTimes
        sqla_session = session


class StopsSchema(ModelSchema):
    class Meta:
        include_fk = True
        model = Stops
        sqla_session = session


class DatabaseUpdate(Base):
    __tablename__ = "database_update"

    id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    last_update_attempt = Column(DateTime())
    table_name = Column(String)
    successful = Column(Boolean())
    exception = Column(String)

    def __repr__(self):
        return (
            f"<{self.__class__.__name__}(last_database_update={self.last_update_attempt}, "
            f"table_name={self.table_name}, successful={self.successful})>"
        )


def create_tables(engine: Engine):
    engine.execute("CREATE VIRTUAL TABLE stops_virtual USING FTS5(id, stop_id, stop_name)")
    Base.metadata.create_all(engine)
