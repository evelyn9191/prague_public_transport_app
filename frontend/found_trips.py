import datetime
from datetime import date

import pytz
from KivyCalendar.calendar_ui import DatePicker
from kivy.core.window import Window
from kivy.garden.datetimepicker import DatetimePicker
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import ScreenManager, Screen

from search_stops.data_types import SearchInput

Builder.load_file("found_trips.kv")


# Window.clearcolor = .3, .3, .3, 1


class TransportSearchApp(App):
    def build(self):
        return FoundTrips()


class FoundTrips(BoxLayout):
    def get_trip(self, trip_id: str):
        return trip_id


class TripData(BoxLayout):
    pass
    # id = Column(Integer, primary_key=True, autoincrement=True)  # noqa: A003
    # route_id = Column(String)
    # service_id = Column(String)
    # trip_id = Column(String, ForeignKey("stop_times.trip_id"))
    # trip_headsign = Column(String)
    # trip_short_name = Column(String)
    # shape_id = Column(String)
    # wheelchair_accessible = Column(Integer)
    # block_id = Column(Integer)
    # direction_id = Column(Integer)
    # bikes_allowed = Column(Integer)
    # exceptional = Column(Integer)
    # trip_operation_type = Column(Integer)


class Header(BoxLayout):
    pass


class BackButton(Button):
    pass


if __name__ == "__main__":
    TransportSearchApp().run()
